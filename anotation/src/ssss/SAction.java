package ssss;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import struts.action.Action;
import struts.form.ActionAnotation;
import struts.form.ActionForm;
import struts.form.ActionsAnotation;

public class SAction implements Action {

	@Override
	@ActionsAnotation(path="/useradd",
		beanName="",
		actionClass="ssss.SAction",
		formClasss="ssss.SForm",
		mapurl={@ActionAnotation(name="shibai",value="/view/UserFailder.jsp"),@ActionAnotation(name="chenggong",value="/view/UserAddSuccess.jsp")})
	public String execute(HttpServletRequest request, ActionForm form,
			Map<String, String> actionForward) {
		
		String url = "shibai";
		SForm sForm = (SForm)form;
		if(sForm.getName().equals("wdq")){
			url = "chenggong";
		}
		
		return actionForward.get(url);
	}

}
